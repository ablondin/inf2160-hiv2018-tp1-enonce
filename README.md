# Travail pratique 1 : Énumération de sous-arbres induits en Haskell

Dans ce premier travail pratique, vous devrez compléter l'implémentation de
trois modules Haskell permettant d'énumérer tous les sous-arbres induits
(c'est-à-dire les [sous-graphes
induits](https://en.wikipedia.org/wiki/Induced_subgraph) qui sont des arbres)
d'un graphe simple donné.

Le travail doit être réalisé **seul**.

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- Se **familiariser** avec le paradigme de programmation **fonctionnel** par
  l'intermédiaire du langage Haskell;
- Exploiter au maximum la **simplicité** d'un langage fonctionnel;
- Proposer des implémentations **différentes** des langages impératifs et
  orientés objet lorsque la situation s'y prête;
- Exploiter au maximum la **récursivité** des fonctions et des types de
  données.

## Contexte théorique

En théorie des graphes, il est fréquent d'étudier les *sous-graphes* qui
apparaissent dans un graphe, puisqu'ils peuvent révéler des informations très
précieuses sur la structure complète du graphe. À titre d'exemple, c'est la
présence des sous-graphes `K5` (le graphe complet de 5 sommets) ou `K33` (le
graphe biparti complet de 3 sommets d'une part et 3 sommets d'autre part) qui
permet de déterminer si un graphe est
[planaire](https://en.wikipedia.org/wiki/Planar_graph) ou non, c'est-à-dire si
on peut le dessiner sur une feuille de papier sans lever le crayon.

Dans le cadre de ce travail pratique, nous nous intéressons aux sous-graphes
induits qui sont des arbres, appelés *sous-arbres induits*. On définit un
*sous-arbre induit* de la façon suivante.

**Définition**. Soit `G = (V,E)` un graphe simple (`V` est l'ensemble de
sommets et `E` l'ensemble d'arêtes) et `U` un sous-ensemble de sommets. Le
graphe `G[U] = (U,E')` est appelé *sous-arbre induit* par `U` si

1. Pour toute paire de sommets `{u,v}` telle que `u` et `v` sont dans `U` et
   que l'arête `{u,v}` est dans `G`, alors on doit forcément prendre l'arête
   `{u,v}` dans `U` également.
2. `G[U]` est connexe;
3. `G[U]` est acyclique (il ne contient pas de cycles);

Considérons les trois images suivantes qui représentent la roue `W7` (la roue
sur `7 + 1` sommets)

| La roue `W7`           | Un sous-arbre induit        | Pas un sous-arbre induit     |
| :-------------------:  | :-------------------------: | :--------------------------: |
| ![](images/wheel7.png) | ![](images/wheel7-0136.png) | ![](images/wheel7-01346.png) |

Les sommmets en verts dans l'image du centre forment bien un sous-arbre induit
puisqu'ils sont connexes et acycliques. Par contre, ceux de l'image de droite
forment un sous-graphe induit, mais pas un sous-arbre induit puisqu'ils ne sont
pas acycliques (les sommets `0`, `3` et `4` forment un cycle).

Le but final de votre travail est de produire un arbre qui permet d'énumérer
tous les sous-arbres induits d'un graphe donné. À noter qu'il peut y en avoir
un très grand nombre (exponentiel), de sorte qu'il est préférable de se
restreindre à des graphes de petite taille. Par exemple, voici l'arbre que vous
obtiendrez pour le graphe `C4` (le cycle de `4` sommets):

![](images/cycle4-tree.png)

En effet, il suffit d'examiner les **feuilles** de cet arbre pour y repérer les
13 sous-arbres induits du graphe `C4`, qui sont donnés par les ensembles `{}`,
`{1}`, `{2}`, `{3}`, `{4}`, `{1,2}`, `{2,3}`, `{3,4}`, `{4,1}`, `{1,2,3}`,
`{2,3,4}`, `{3,4,1}` et `{4,1,3}`.

L'arbre est simplement construit en choisissant un sommet et en allant à gauche
si on l'inclut (en vert) ou à droite si on décide de l'exclure (en rouge). En
particulier, lorsqu'on décide d'inclure un sommet, il est possible que cela
entraîne automatiquement l'exclusion d'autres sommets, ce qui explique pourquoi
l'arbre n'est pas parfaitement équilibré.

Afin de bien tester votre projet, vous pourrez effectuer des tests sur quelques
familles de graphes classiques:

- Les [graphes complets](https://en.wikipedia.org/wiki/Complete_graph);
- Les [cycles](https://en.wikipedia.org/wiki/Cycle_graph);
- Les [roues](https://en.wikipedia.org/wiki/Wheel_graph);
- Les [graphes bipartis
  complets](https://en.wikipedia.org/wiki/Complete_bipartite_graph);
- Le [graphe de Petersen](https://en.wikipedia.org/wiki/Petersen_graph);

## Description du travail

Rendez-vous sur https://gitlab.com/ablondin/inf2160-hiv2018-tp1, où se trouvent
déjà différents fichiers, dont trois modules Haskell non implémentés (`Graph`,
`Configuration` et `ISTree`). Avant même de vous lancer dans l'implémentation
des différents modules, prenez le temps de bien vous familiariser avec la
structure de l'interaction entre tous les modules, même s'ils sont incomplets.

Votre travail consistera à compléter les implémentations des trois modules. Il
est recommandé de procéder dans l'ordre suivant, même s'il est possible que
vous alterniez entre différents modules:

1. `Graph.hs`: Un module qui permet de représenter un graphe simple (non
   orienté);
2. `Configuration.hs`: Un module qui permet de représenter un sous-arbre
   induit, en étiquetant les sommets selon qu'ils sont *inclus*, *exclus* ou
   *libres*.
3. `ISTree.hs`: Un module qui permet de représenter sous forme d'arbre binaire
   et de façon unique tous les sous-arbres induits d'un graphe donné.

## Tests automatiques

Si vous avez installé le logiciel
[Doctest](https://github.com/sol/doctest#readme), il est possible de lancer une
suite de tests de façon automatique à l'aide de la commande
```sh
make test
```
Évidemment, comme l'implémentation est presque complètement absente lorsque
vous débutez, la plupart des tests échoueront. Au fur et à mesure que vous
avancerez dans votre travail, vous pourrez donc valider l'évolution en tout
temps.

**Attention:** Même si vous réussissez tous les tests, cela ne signifie pas que
votre projet est parfait et sans bogue. Il est important d'ajouter vos propres
tests afin de vous assurer que vous avez prévu tous les cas possibles.
Plusieurs des tests utilisés pour corriger votre travail seront différents.

## Génération de la documentation

Si vous avez installé le logiciel [Haddock](https://www.haskell.org/haddock/),
vous pouvez en tout temps générer la documentation du projet. Pour cela, il
suffit d'entrer la commande
```sh
make doc
```
Ensuite, il suffit d'ouvrir le fichier `doc/index.html`.

## Contraintes

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes:

- Votre projet doit être un clone (*fork*) **privé** du projet
  https://gitlab.com/ablondin/inf2160-hiv2018-tp1. L'adjectif **privé** est
  très important si vous ne voulez pas que d'autres étudiants accèdent à votre
  solution (**pénalité de 50%**).
- Vous devez donner accès en mode `Developer` à l'utilisateur `ablondin` avant
  la date de remise (**pénalité de 50%**).
- Votre programme doit **compiler** lorsqu'on entre la commande `make`
  (**pénalité de 100%**).
- Il est interdit de modifier les **signatures** des fonctions et les
  **définitions** des types fournis (**pénalité de 100%**). En revanche, rien
  ne vous empêche d'ajouter des fonctions auxiliaires si nécessaires pour vous
  aider dans votre implémentation.

## Compléments sur l'utilisation du logiciel Git

Si vous avez commencé à travailler sur le projet et que j'ai apporté des
modifications au dépôt public, il est possible de les récupérer facilement à
l'aide de Git.

J'utiliserai la terminologie suivante:
- *local* est votre copie locale du dépôt sur laquelle vous travaillez. Il peut
  s'agir de votre machine personnelle, d'une machine dans les laboratoires de
  l'UQAM ou encore du serveur Java (si vous travaillez en connexion SSH).
- *origin* est le dépôt obtenu sur GitLab après avoir fait un *fork* (clone).
- *upstream* est le dépôt public qui appartient à l'utilisateur `ablondin`
  (moi-même).

### Établir le lien entre *local* et *upstream*

En principe, vous avez actuellement déjà un lien entre le dépôt *local* et le
dépôt *origin*, qui a été établi lorsque vous avez cloné le projet sur votre
machine personnelle (ou sur le serveur Java). Vous pouvez voir comment ce lien
est établi en entrant la commande
```sh
git remote -v
```
qui indique qu'il y a un dépôt distant (en anglais, *remote*) qui s'appelle
*origin* et qui est votre sauvegarde personnelle sur GitLab. Vous devez donc
appeler un deuxième dépôt distant qui s'appellera *upstream*. Pour cela, il
suffit d'entrer la commande
```sh
git remote add upstream https://gitlab.com/ablondin/inf2160-hiv2018-tp1.git
```
pour établir le lien avec mon dépôt. Notez que le protocole ici est HTTPS et
non SSH, car vous ne pousserez pas de modifications sur mon dépôt, vous allez
seulement en récupérer.

Si jamais vous vous êtes trompés en entrant la commande, il est toujours
possible de supprimer un dépôt distant (tapez la commande `git remote --help`
pour plus de détails).

### Récupérer la version la plus à jour

Avant de récupérer les modifications, assurez-vous que votre dépôt est
*propre*, c'est-à-dire que le résultat de la commande
```sh
git status
```
indique qu'il n'y a aucune modification en cours qui n'a pas encore été
*committée*.

Je suppose ici que vous n'êtes pas trop familiers avec les branches (sinon,
vous n'auriez sans doute pas besoin de lire cette partie). Comme ce cours ne
porte pas sur le logiciel Git spécifiquement, vous pouvez simplement récupérer
les modifications en entrant la commande
```sh
git pull upstream master
```
Attention, il est possible que cette commande entraîne un conflit. Si c'est le
cas, je vous encourage à lire un tutoriel qui explique comment résoudre un
conflit (c'est une activité très commune quand on utilise Git). Par exemple,
vous pouvez regarder [ce
lien](http://alainericgauthier.com/git/gerer_les_conflits_de_fusion).

### Capsules vidéos

Si vous préférez avoir des explications plus "visuelles" de l'utilisation de
Git, vous pouvez accéder à trois capsules vidéos que j'ai tournées il y a
quelques années, dans lesquelles j'expliquais aux étudiants comment faire
certaines modifications sous Git.  Noter qu'elles sont un peu plus avancées que
celle que j'explique plus haut et que vous devrez adapter certaines parties à
ce travail pratique.  Les vidéos se trouvent
[ici](https://uqam.hosted.panopto.com/Panopto/Pages/Sessions/List.aspx?folderID=1a2b1115-e453-4c6d-b972-a87a0042b8b2).

### Intégration continue

Il est possible d'activer l'intégration continue à votre dépôt. Pour cela,
il suffit de récupérer le fichier [.gitlab-ci.yml](files/.gitlab-ci.yml)
disponible dans le dépôt de l'énoncé et de le placer à la racine de votre
projet. Merci à @marcandjulien de m'avoir donné la permission de partager sa
solution.

L'intégration continue permet de tester de façon automatique votre code chaque
fois que vous poussez des modifications sur le dépôt. Pour plus d'informations,
veuillez consulter la
[documentation](https://about.gitlab.com/features/gitlab-ci-cd/).

Notez que cet ajout est optionnel lors de la remise du travail pratique.

## Remise

Votre travail sera remis en deux parties de la façon suivante:

- L'implémentation du module `Graph` doit être complétée au plus tard le **16
  février 2018** à **23h59**. Cette partie comptera pour **60 points/200
  points**. À partir de minuit, une pénalité de **2 points par heure** de
  retard sera appliquée.
- L'implémentation des modules `Graph`, `Configuration` et `ISTree` doivent
  être complétées au plus tard le **1er mars 2018** à **23h59**. Cette partie
  comptera pour **140 points/200 points**. À partir de minuit, une pénalité de
  **2 points par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux seront corrigés sur le serveur Java. Vous devez donc vous assurer
que votre programme fonctionne **sans modification** sur celui-ci.

## Barème

Les critères suivants seront pris en compte dans l'évaluation.

### Remise 1 (60 points)

Les 17 fonctions du module `Graph` dont la définition actuelle est
```haskell
error "À compléter"
```
devront être implémentées. Seule la fonctionnalité sera évaluée lors de cette
première remise, à l'aide de tests similaires à ceux qui sont déjà fournis dans
les exemples (et qui peuvent être lancés avec `make test`).

### Remise 2 (140 points)

L'implémentation des trois modules (`Graph`, `Configuration` et `ISTree`) devra
être complétée et fonctionnelle. En outre, vous devrez documenter toutes les
fonctions auxiliaires que vous avez définies et vous devrez compléter le
fichier `README.md`.

Voici la ventilation plus précise de l'attribution des points:

- **Fonctionnalité (80 points)**: Les fonctions ont le comportement auquel on
  s'attend. La fonctionnalité sera vérifiée à l'aide de tests automatiques
  similaires à ceux déjà disponibles dans les *docstrings*. Comme pour la
  première partie, il s'agit de remplacer les définitions de la forme

    ```haskell
    error "À compléter"
    ```

- **Style de programmation (30 points)**: Le style de programmation sera pris
  en considération. Voici quelques aspects qui seront évalués:
  
    * Le code suit le [standard recommandé pour Haskell](https://wiki.haskell.org/Programming_guidelines);
    * Le code est bien indenté;
    * Le code ne contient pas de caractères problématiques (encodage UTF8);
    * Éviter les caractères de tabulations qui influencent l'indentation selon
      l'éditeur de texte utilisé;
    * Le code est bien aéré autour des opérateurs et des délimiteurs.
    * Le code ne contient pas de longues lignes;
    * Le style du code est cohérent avec celui déjà présent;
    * Le paradigme fonctionnel est bien exploité (ne pas se rabattre sans cesse
      sur des patrons impératifs/objets).
    * Le code ne redéfinit pas des fonctions déjà implémentées dans des
      bibliothèques standards (comme `Data.List` et `Data.Map`, par exemple).
    * L'implémentation est aussi courte que possible;
    * L'implémentation est lisible.

- **Fichier README (30 points)**: 

    * Le contenu du fichier est complet et exhaustif, selon les sections
      proposées.
    * Il exploite correctement le format Markdown (bouts de code, pas d'italique
      et de gras partout, découpage en sections, utilisation de listes à puces);
    * Il ne contient pas de fautes d'orthographe, de frappe ou d'anglicismes (à
      moins que les mots en anglais soient identifiés en italique).
